taktuk (3.7.7-3) unstable; urgency=medium

  * New upstream page, fix debian/watch and homepage
  * Add Rules-Requires-Root: no
  * Add patch 0003-Remove-ISO-8859-text-in-AUTHORS.patch
  * Refresh patch
  * Bump DH compat level to 13
  * Bump Standards-Version to 4.6.1. No changes needed.
  * Update date in debian/copyright

 -- Lucas Nussbaum <lucas@debian.org>  Thu, 28 Jul 2022 16:45:26 +0000

taktuk (3.7.7-2) unstable; urgency=medium

  * debian/rules: remove trailing whitespace
  * debian/copyright: Switch to HTTPS
  * debian/control: update Vcs-* to point to salsa
  * Bump Standards-Version to 4.3.0
  * Bump dh compat level to 12
  * Drop dh --with autotools_dev
  * Use dh_missing
  * Drop b-dep on autotools-dev
  * debian/watch: use https

 -- Lucas Nussbaum <lucas@debian.org>  Thu, 18 Jul 2019 15:38:45 +0200

taktuk (3.7.7-1) unstable; urgency=medium

  * New upstream version 3.7.7
  * Refresh patch: 0002-Fix-some-typos-in-docs.patch
  * Bump Standards-Version to 4.1.1

 -- Lucas Nussbaum <lucas@debian.org>  Tue, 31 Oct 2017 11:34:02 +0100

taktuk (3.7.6-2) unstable; urgency=medium

  * Fix VCS fields to use secure URIs
  * Bump Standards-Version to 4.0.0
  * Add patch 0002-Fix-some-typos-in-docs.patch
  * Add patch metadata in force-perl-directories.diff

 -- Lucas Nussbaum <lucas@debian.org>  Wed, 26 Jul 2017 08:15:39 +0200

taktuk (3.7.6-1) unstable; urgency=medium

  * New upstream release.
    + avoid spurious warnings
    + refresh patches (and remove upstream applied ones)
  * Dump Standards-Version (no changes needed)
  * This rebuild should fix #760204 (libtaktuk-1-dev: arch-dependent file in
    "Multi-Arch: same" package): it was due to an old perl version installed
    on the powerpc builder (Closes: #760204)

 -- Vincent Danjean <vdanjean@debian.org>  Mon, 20 Jul 2015 16:35:17 +0200

taktuk (3.7.5-1) unstable; urgency=medium

  * New upstream release.
  * Update my email address.
  * Bump Standards-Version to 3.9.5. No changes needed.
  * Move the package to Git. Update Vcs-*.

 -- Lucas Nussbaum <lucas@debian.org>  Sun, 31 Aug 2014 22:59:29 -0700

taktuk (3.7.4-1) unstable; urgency=low

  * New upstream release
  * Refresh packaging
    + refresh quilt patches
    + bump Standards-Version to 3.9.3 (no changes required)
    + use --fail-missing for dh_install to be sure to not miss a file
    + fix the use of Breaks/Conflicts and removed unused substvars
    + fix typos in Descriptions
  * Switch to "3.0 quilt" source format
  * Add multiarch support
    + bump dh compat to level 9

 -- Vincent Danjean <vdanjean@debian.org>  Mon, 11 Jun 2012 10:45:16 +0200

taktuk (3.7.3-3) unstable; urgency=low

  * Fix "(taktuk_3.7.3-2/avr32): FTBFS: Outdated config.{sub,guess}"
    using "dh --with autotools-dev" (Closes: #574751)
  * Fix debian/*.install files to avoid empty library package :-(
  * Add a package for perl modules

 -- Vincent Danjean <vdanjean@debian.org>  Tue, 23 Mar 2010 14:13:01 +0100

taktuk (3.7.3-2) unstable; urgency=low

  * Fix typo in package names

 -- Vincent Danjean <vdanjean@debian.org>  Fri, 12 Mar 2010 17:38:20 +0100

taktuk (3.7.3-1) unstable; urgency=low

  * New upstream version.
  * Switch to debhelper 7
  * Switch Lucas and myself as maintainer/uploader
  * Bump Standard-Version

 -- Vincent Danjean <vdanjean@debian.org>  Fri, 12 Mar 2010 16:52:04 +0100

taktuk (3.6.3-2) UNRELEASED; urgency=low

  * NOT RELEASED YET

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Sat, 04 Apr 2009 14:20:14 +0200

taktuk (3.6.3-1) unstable; urgency=low

  * New upstream version.
  * Upgraded to policy 3.8.1 ; no changes required.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Sat, 04 Apr 2009 13:57:13 +0200

taktuk (3.6.2-2) unstable; urgency=low

  * Also install /usr/share/man/man3/taktukcomm.3.gz.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Fri, 09 Jan 2009 11:15:42 +0100

taktuk (3.6.2-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Fri, 09 Jan 2009 09:11:00 +0100

taktuk (3.6.1-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Sat, 28 Jun 2008 21:09:46 +0200

taktuk (3.6.1~beta2-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue, 17 Jun 2008 13:25:56 +0200

taktuk (3.6.1~beta1-1) unstable; urgency=low

  * New upstream release
  * Updated to policy 3.8.0 (no changes needed).

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue, 17 Jun 2008 00:43:53 +0200

taktuk (3.6-2) unstable; urgency=low

  * Fixed Vcs-*: they must point to the directory containing the debian/
    dir, not the one containing trunk/
  * Split changelog line too long (caused lintian warning).
  * Changed section of libtaktuk2-dev (devel -> libdevel). Agrees with
    FTP masters.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue, 15 Jan 2008 12:58:05 +0100

taktuk (3.6-1) unstable; urgency=low

  * New upstream release
    The following modification make TakTuk 3.6 incompatible with earlier
    versions:
    - changed commands set:
      - grouped *_input and close commands into variants of input command
      - grouped network_state and resign into variants of network command
    - improved option command: new syntaxic form and now able to deploy nodes
    - changed internal information transport system: avoids some deadlock
      situations and should be more efficient
    - added network renumber and update to assign logical numbers to new nodes
    - added get function to perl communication interface
    - completely rewrote and changed C-interface
    - added -M option to prevent option inherintence
    - rewrote tests to make them human readable
    - added new tests (multithreaded, put/get)
    - made quit command usable in all parts of the tree
    - added new tests (dynamic)
    - actually made options set by environment propagate themselves
    - added new form of environment option setting (local setting)
    - stolen connectors use the connector command of the thief
    - added state information for files tranfer
  * [debian/control]
    - remove Homepage in descriptions
    - add Homepage field in source stanza
    - use fields Vcs-* instead of Xs-Vcs-*
    - conflict with kanif << 1.2
    - remove libtaktuk0{,-dev} and add libtaktuk2{,-dev} packages
    - libtaktuk2-dev conflict and replace libtaktuk0-dev
    - upgrade to policy 3.7.3 (no change required)

 -- Vincent Danjean <vdanjean@debian.org>  Mon, 03 Dec 2007 12:39:50 +0100

taktuk (3.5.2-1) unstable; urgency=low

  * New upstream release.
  * Switch to collab-maint repository.
    + Fixed XS-VCS addresses.
  * Added Vincent Danjean as co-maintainer.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Mon, 22 Oct 2007 09:39:42 +0200

taktuk (3.5-2) unstable; urgency=low

  * New upload to fix broken 3.5-1.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Sat, 13 Oct 2007 17:09:07 +0200

taktuk (3.5-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Fri, 12 Oct 2007 19:41:19 +0200

taktuk (3.3-1) unstable; urgency=low

  * New upstream release. Closes: #443693.
  * Conflict with kanif << 1.1.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Fri, 21 Sep 2007 18:39:08 +0200

taktuk (3.2.5-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Wed, 05 Sep 2007 18:38:54 +0200

taktuk (3.2.4-1) unstable; urgency=low

  * New upstream release.
  * Added /usr/lib/pkgconfig/taktuk.pc.
  * taktuk now suggests kanif.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Wed, 05 Sep 2007 11:28:48 +0200

taktuk (3.2.3-2) unstable; urgency=low

  * Improved description (mentioned 'ssh').

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Fri, 20 Jul 2007 11:18:53 +0200

taktuk (3.2.3-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Thu, 05 Jul 2007 13:10:36 +0000

taktuk (3.2.2-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Wed, 20 Jun 2007 22:56:54 +0200

taktuk (3.2.1-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Fri, 15 Jun 2007 20:14:01 +0200

taktuk (3.2-2) unstable; urgency=low

  * Added XS-Vcs-* fields.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Wed, 13 Jun 2007 18:33:14 +0200

taktuk (3.2-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Mon, 04 Jun 2007 14:49:44 +0200

taktuk (3.1-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Thu, 12 Apr 2007 14:24:05 +0200

taktuk (3.0.2-1) unstable; urgency=low

  * New upstream release.
  * Minor fix to the package description.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Fri, 26 Jan 2007 14:36:16 +0100

taktuk (3.0-1) unstable; urgency=low

  * Initial release. (Closes: #401195)

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Thu, 30 Nov 2006 16:12:17 +0100
