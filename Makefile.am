###############################################################################
#                                                                             #
#  TakTuk, a middleware for adaptive large scale parallel remote executions   #
#  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        #
#                                                                             #
#  This program is free software; you can redistribute it and/or modify       #
#  it under the terms of the GNU General Public License as published by       #
#  the Free Software Foundation; either version 2 of the License, or          #
#  (at your option) any later version.                                        #
#                                                                             #
#  This program is distributed in the hope that it will be useful,            #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#  GNU General Public License for more details.                               #
#                                                                             #
#  You should have received a copy of the GNU General Public License          #
#  along with this program; if not, write to the Free Software                #
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA #
#                                                                             #
#  Contact: Guillaume.Huard@imag.fr                                           #
#           Laboratoire LIG - ENSIMAG - Antenne de Montbonnot                 #
#           51 avenue Jean Kuntzmann                                          #
#           38330 Montbonnot Saint Martin                                     #
#           FRANCE                                                            #
#                                                                             #
###############################################################################

.PHONY: commit clean_doc

# flags to aclocal if Makefile rules trigger it
ACLOCAL_AMFLAGS=-I acinclude

PERL_MODULES=Perl-Module/lib/TakTuk.pm Perl-Module/lib/TakTuk/Select.pm
PERL_MODULES_FILES= Perl-Module/Makefile.PL Perl-Module/MANIFEST.SKIP \
                    Perl-Module/lib/TakTuk/Pilot.pm DISCLAIMER

all-am: $(PERL_MODULES) taktuk-light modules_make

docdir = ${datadir}/doc
pkgdocdir = ${docdir}/@PACKAGE@
htmldocdir = ${pkgdocdir}/html
pkgconfigdir = $(libdir)/pkgconfig

dist_bin_SCRIPTS = taktuk
pkgconfig_DATA= taktuk.pc
pkgdoc_DATA= COPYING AUTHORS README NEWS ChangeLog

SUBDIRS=C-Module . tests

if TAKTUK_GENDOC_HTML
nodist_htmldoc_DATA=taktuk.html taktukcomm.html taktuk_module.html \
                  taktuk_pilot.html
endif
if TAKTUK_GENDOC_MAN
nodist_man1_MANS=taktuk.1
nodist_man3_MANS=taktukcomm.3
endif


if TAKTUK_NOPERL
modules_make:

install-exec-hook:
	@echo;echo WARNING: perl is required for taktuk execution;
	@     echo "        and full installation";

else
PERLSRC=@abs_top_srcdir@/Perl-Module
Perl-Module/Makefile: Perl-Module/Makefile.PL Perl-Module/lib/TakTuk.pm Perl-Module/lib/TakTuk/Select.pm
	set -e ; \
	cd Perl-Module ; \
	if [ ! -f Makefile.PL ] \
		&& [ -f $(PERLSRC)/Makefile.PL ]; then \
		mkdir -p lib/TakTuk ;\
		ln -s $(PERLSRC)/MANIFEST.SKIP . ; \
		ln -s $(PERLSRC)/lib/TakTuk/Pilot.pm lib/TakTuk ; \
		ln -s $(PERLSRC)/Makefile.PL . ; \
	fi ;\
	perl Makefile.PL PREFIX=${prefix}

modules_make: Perl-Module/Makefile
	$(MAKE) -C Perl-Module

install-exec-hook: Perl-Module/Makefile
	$(MAKE) -C Perl-Module install

clean-local: Perl-Module/Makefile
	$(MAKE) -C Perl-Module distclean
	$(MAKE) clean-generic

endif

taktuk.1: taktuk.pod
	$(POD2MAN) --name=TAKTUK --section=1 \
	     --center="TakTuk Deployment Engine" $^ >$@

taktukcomm.3: taktukcomm.pod
	$(POD2MAN) --name=TAKTUK --section=3 \
	     --center="TakTuk Library Functions Manual" $^ >$@

taktuk.html taktukcomm.html taktuk_module.html: %.html: %.pod
	$(POD2HTML) --title="TakTuk" $^ >$@
	$(RM) pod2htmd.tmp pod2htmi.tmp

taktuk_pilot.html: Perl-Module/lib/TakTuk/Pilot.pm
	$(POD2HTML) --title="TakTuk" $^ >$@
	$(RM) pod2htmd.tmp pod2htmi.tmp

CLEANFILES = taktuk-light
taktuk-light: taktuk
	echo "#!/usr/bin/perl" >$@
	sed -e "s/^ *//g;s/  */ /g;s/^ *#.*$$//g;/^$$/d" $^ >>$@
	chmod u+x $@

CLEANFILES += $(PERL_MODULES)
Perl-Module/lib/TakTuk.pm: taktuk DISCLAIMER taktuk_module.pod
	mkdir -p $(dir $@)
	cat $(filter %DISCLAIMER,$^) >$@
	./$(filter %taktuk,$^) -p TakTuk >>$@
	cat $(filter %taktuk_module.pod,$^) >>$@

Perl-Module/lib/TakTuk/Select.pm: taktuk DISCLAIMER
	mkdir -p $(dir $@)
	cat $(filter %DISCLAIMER,$^) >$@
	./$(filter %taktuk,$^) -p TakTuk::Select >>$@

DISTCLEANFILES = $(nodist_htmldoc_DATA) $(nodist_man1_MANS) $(nodist_man3_MANS)

CLEANFILES += $(PERL_MODULES)

EXTRA_DIST = taktukcomm.pod taktuk.pod taktuk_module.pod \
	     $(PERL_MODULES_FILES) Bugs bootstrap API-versions.txt

check_SCRIPTS=check_versions
TESTS=$(check_SCRIPTS)

# no uninstall check as ExtUtils::MakeMaker does not support uninstall
distuninstallcheck:
	@:
