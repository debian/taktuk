/******************************************************************************
*                                                                             *
*  TakTuk, a middleware for adaptive large scale parallel remote executions   *
*  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        *
*                                                                             *
*  This program is free software; you can redistribute it and/or modify       *
*  it under the terms of the GNU General Public License as published by       *
*  the Free Software Foundation; either version 2 of the License, or          *
*  (at your option) any later version.                                        *
*                                                                             *
*  This program is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with this program; if not, write to the Free Software                *
*  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA *
*                                                                             *
*  Contact: Guillaume.Huard@imag.fr                                           *
*           ENSIMAG - Laboratoire d'Informatique de Grenoble                  *
*           51 avenue Jean Kuntzmann                                          *
*           38330 Montbonnot Saint Martin                                     *
*                                                                             *
******************************************************************************/

#include <taktuk.h>
#include <stdio.h>

int main()
  {
    char buffer[1024];
    size_t length;
    char sample_string[128] = "Salut, toi";
    unsigned long rank, from;
    int result;

    printf("Hellooo...\n");
    result = taktuk_get("rank", &rank);
    if (result)
      {
        fprintf(stderr,"Invalid rank: %s, do you use TakTuk ?\n",
                                                     taktuk_error_msg(result));
        return 1;
      }
    printf("I'm process %lu\n", rank);

    if (rank == 1)
      {
        result = taktuk_send(2, TAKTUK_TARGET_ANY, sample_string, 128);
        if (result)
          {
            printf("Error in %lu : %s\n", rank, taktuk_error_msg(result));
          }
      }
    else if (rank == 2)
      {
        fflush(stdout);
        result = taktuk_recv(&from, buffer, &length, NULL);
        if (result)
          {
            printf("Error in %lu : %s\n", rank, taktuk_error_msg(result));
          }
        else
          {
            printf("Received : %s of length %lu from %lu\n", buffer,
                                                  (unsigned long) length, from);
          }
      }
    else
      {
        printf("Got nothing to do\n");
      }
    return 0;
  }
